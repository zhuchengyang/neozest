#pragma once
#include "output/zestBaseVisitor.h"

class  CompileVisitor : public zestBaseVisitor {
public:

  std::any visitFunc_head_without_param(zestParser::Func_head_without_paramContext *ctx);
  std::any visitFunc_head_with_param(zestParser::Func_head_with_paramContext *ctx);
  std::any visitParam_def(zestParser::Param_defContext *ctx);
  std::any visitFunc_with_impl(zestParser::Func_with_implContext *ctx);
  std::any visitFunc_without_impl(zestParser::Func_without_implContext *ctx);
  std::any visitVal_with_brackets(zestParser::Val_with_bracketsContext *ctx);
  std::any visitInstant_value(zestParser::Instant_valueContext *ctx);
  std::any visitFunction_call(zestParser::Function_callContext *ctx);
  std::any visitAssignment(zestParser::AssignmentContext *ctx);
  std::any visitVariant(zestParser::VariantContext *ctx);
  std::any visitCode_block(zestParser::Code_blockContext *ctx);
  std::any visitMultiply_divide(zestParser::Multiply_divideContext *ctx);
  std::any visitIf_without_else(zestParser::If_without_elseContext *ctx);
  std::any visitIf_with_else(zestParser::If_with_elseContext *ctx);
  std::any visitFunction_definition(zestParser::Function_definitionContext *ctx);
  std::any visitAdd_subtract(zestParser::Add_subtractContext *ctx);
  std::any visitParams(zestParser::ParamsContext *ctx);
  std::any visitInstant(zestParser::InstantContext *ctx);

};

