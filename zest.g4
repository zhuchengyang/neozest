grammar zest;

func_head: 'fun' IDENTIFIER '(' ')' ':' val                             # func_head_without_param
         | 'fun' IDENTIFIER '(' param_def (',' param_def)*? ')' ':' val # func_head_with_param
         ;

param_def: IDENTIFIER ':' val
         ;

func: func_head '=>' val ';'    # func_with_impl
    | func_head ';'             # func_without_impl
    ;

// 'val' can be instant, expression, type, or control_flow.

val_with_brackets: '{' (val ';'?)*? '}'
                 ;

val: val_with_brackets                                      # code_block
   | func                                                   # function_definition
   | val ('*' | '/') val                                    # multiply_divide
   | val ('+' | '-') val                                    # add_subtract
   | 'if' val val_with_brackets 'else' val_with_brackets    # if_with_else
   | 'if' val val_with_brackets                             # if_without_else
   | IDENTIFIER                                             # variant
   | val '=' val                                            # assignment
   | IDENTIFIER '(' params? ')'                             # function_call
   | instant                                                # instant_value
   ;

params: val (',' val)*
      ;

instant: NUMBER
       ; // because I'm lazy.

NUMBER: [1-9][0-9]*;

IDENTIFIER: [a-zA-Z_][a-zA-Z0-9_]*;

WS: [ \n\t\r]+ -> skip;
